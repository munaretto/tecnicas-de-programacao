/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a1861875
 */

import java.util.Date;
import java.util.GregorianCalendar;

public class Pratica31 {
    private static String meuNome = "Pedro Munaretto".toUpperCase(); // 6 15 28
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1997,6,14); //mês vai de 0 a 11
    private static GregorianCalendar diaAtual = new GregorianCalendar();
    private static long tempoPassado = diaAtual.getTime().getTime() - dataNascimento.getTime().getTime();
    private static long batatinha = tempoPassado/86400000;
    private static Date inicio,fim;
    
    public static void main(String[] args){
        inicio = new Date();
        System.out.println(meuNome);
        System.out.println(meuNome.charAt(6) + meuNome.toLowerCase().substring(7) + ", " + meuNome.charAt(0) + '.');
        System.out.println(batatinha);
        fim = new Date();
        System.out.println(fim.getTime()-inicio.getTime());
    }
}
